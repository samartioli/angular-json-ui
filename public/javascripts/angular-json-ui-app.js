angular.module('angular-json-ui', ['angular-json-ui-directives'])
    .controller('MainController', ['$scope', '$log', '$element', function($scope, $log, $element) {

        $scope.$log = $log;

        $scope.testObject = {
            z: ['a','b'],
            y: {a:1},
            // a:true,
            // b:false,
            // c: {
            //     aa:'aa',
            //     bb:'bb',
            //     cc:'cc',
            //     d: true,
            //     e: false,
            //     f: {
            //         aaa:'aaa',
            //         bbb:'bbb',
            //         ccc:'ccc',
            //     }

            // },
            // d: 'd-string',
            // e: 'f-string',
            // f: ['a-array', 'b-array', 'c-array'],
            // g: [{gg:'gg'}, {hh: true}, {ii: false}],
            // h: 123,
            // i: '456',
            // zz: [{a:'a'},'b'],
        };

        console.log($element);

        $scope.printObject = function() {
            console.log($scope.testObject);
        };

        $scope.changeObject = function() {
            $scope.testObject.a = 'asd';
        };

        $scope.collapseAll = function() {
            $element.children('.base').children('ul').find('ul').addClass('ng-hide')
            $element.children('.base').children('ul').find('.ellipsis').removeClass('ng-hide')
        };

        $scope.expandAll = function() {
            $element.children('.base').children('ul').find('ul').removeClass('ng-hide')
            $element.children('.base').children('ul').find('.ellipsis').addClass('ng-hide')
        };

        $scope.edit = function() {
            $('.edit').toggleClass('ng-hide');
        };

        // $scope.$watch(function() {
        //     console.log('watch');
        // });

    }])
;
