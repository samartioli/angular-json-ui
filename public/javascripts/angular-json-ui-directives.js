angular.module('angular-json-ui-directives', [])

    .directive('collection', function($compile) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                collection: '='
            },
            template: "<div><ul></ul></div>",
            controller: function($scope, $element, $attrs, $transclude) {
                $scope.click = function(e) {
                    //console.log(angular.toJson($scope.collection));
                    //$('ul.create').addClass('ng-hide');
                    $(e.target).siblings().toggleClass('ng-hide');
                    e.stopPropagation();
                };
                $scope.create = function(e, kind, isArray) {
                    console.log(angular.toJson($scope.collection));

                    if (isArray) {
                        switch(kind) {
                            case 'object':
                                $scope.collection.push({});
                                break;
                            case 'array':
                                $scope.collection.push([]);
                                break;

                            case 'kv':
                                $scope.collection.push('newValue');
                                break;
                        };
                    } else {
                        switch(kind) {
                            case 'object':
                                $scope.collection['newObject'] = {};
                                break;
                            case 'array':
                                $scope.collection['newArray'] = [];
                                break;

                            case 'kv':
                                $scope.collection['newKey'] = 'newValue';
                                break;
                        };
                    }

                };

            },
            link: function(scope, element, attrs) {

                var html = "<p> ERROR: Collection is neither an object or an array";

                if (angular.isArray(scope.collection)) {
                    element.addClass('type-array');
                    element.prepend('[ <span class="ellipsis ng-hide"></span>');
                    element.append(']');
                    var html = "<member ng-repeat='(key, value) in collection' \
                                    key='key' value='value' collection='collection' isarray='true'>\
                                </member>\
                                <li>\
                                    <i class='edit fa fa-plus-square-o ng-hide' ng-click='click($event)'></i>\
                                    <ul class='create ng-hide'>\
                                        <li ng-click='create($event, \"object\", true)'>Object</li>\
                                        <li ng-click='create($event, \"array\", true)'>Array</li>\
                                        <li ng-click='create($event, \"kv\", true)'>Key/Value</li>\
                                    </ul>\
                                </li>";
                } else if (angular.isObject(scope.collection)) {
                    element.addClass('type-object');
                    element.prepend('{ <span class="ellipsis ng-hide"></span>');
                    element.append('}');
                    var html = "<member ng-repeat='(key, value) in collection' \
                                    key='key' value='value' collection='collection' isarray='false'>\
                                </member>\
                                <li>\
                                    <i class='edit fa fa-plus-square-o ng-hide' ng-click='click($event)'></i>\
                                    <ul class='create ng-hide'>\
                                        <li ng-click='create($event, \"object\", false)'>Object</li>\
                                        <li ng-click='create($event, \"array\", false)'>Array</li>\
                                        <li ng-click='create($event, \"kv\", false)'>Key/Value</li>\
                                    </ul>\
                                </li>";
                }


                $compile(html)(scope, function(cloned, scope) {
                    element.find('ul').append(cloned);
                });

            },
        }
    })

    .directive('label', function() {
        return {
            restrict: "E",
            replace: false,
            scope: {
                text: '='
            },
            template: "{{text}}",
            link: function(scope, element, attrs) {

                element.bind('blur', function(z) {

                    scope.$apply(function() {
                        //console.log('### INFO: Applying Scope to: ' + scope.$id);
                        scope.text = element.html();
                    });

                    scope.$parent.$apply(function() {
                        //console.log('### INFO: Applying Scope to parent: ' + scope.$parent.$id + " from: " + scope.$id);
                        scope.$parent.collection[scope.$parent.key] = scope.$parent.value;
                    });

                });

            }
        }
    })

    .directive('hover', function($compile) {
        return {
            restrict: "E",
            // transclude: true,
            replace: true,
            template: "<div class='hoverable' ng-mouseenter='mouseEnter()' ng-mouseleave='mouseLeave($event)'></div>",
            controller: function($scope, $element, $attrs, $transclude) {

                $scope.toggleCollapse = function() {
                    $element.toggleClass('collapsed');
                    $element.find('ul').toggleClass('ng-hide');
                    $element.find('.ellipsis').toggleClass('ng-hide');
                };

                $scope.mouseEnter = function(e) {
                    $('.hovered').removeClass('hovered');
                    $element.addClass('hovered');
                };

                $scope.mouseLeave = function(e) {
                    if ($(e.toElement).hasClass('hoverable')) {
                        $(e.toElement).addClass('hovered');
                    }
                    if ($(e.toElement.parentElement).hasClass('hoverable')) {
                        $(e.toElement.parentElement).addClass('hovered');
                    }
                };

            },
        }
    })

    .directive('member', function($compile) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                key: '=',
                value: '=',
                collection: '=',
                isarray: '=',
            },
            template: "<li><hover><hover></li>",
            link: function(scope, element, attrs) {

                if (!scope.isarray) {
                    element.children('.hoverable').append("<span class='property'>" + scope.key + "</span>:&nbsp");
                }

                var html = '<p>Empty</p>';

                switch(typeof scope.value) {
                    case 'string':
                        html = '<span class="type-string">"<label contenteditable="true" text=value></label>"</span>';
                        break;
                    case 'number':
                        html = '<span class="type-number"><label contenteditable="true" text=value></label></span>';
                        break;
                    case 'boolean':
                        html = '<span class="type-boolean"><label contenteditable="true" text=value></label></span>';
                        break;
                    case 'object':
                        html = '<div class="collapser" ng-click="toggleCollapse()"></div><collection collection="value"></collection>';
                        break;
                }

                $compile(html)(scope, function(cloned, scope) {
                    element.children('.hoverable').append(cloned).append(',');

                });

            }
        }
    })
;